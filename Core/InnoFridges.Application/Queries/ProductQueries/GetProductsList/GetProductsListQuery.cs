﻿using MediatR;

namespace InnoFridges.Application.Queries.ProductQueries.GetProductsList;

public class GetProductsListQuery : IRequest<ProductsListViewModel>
{
    
}
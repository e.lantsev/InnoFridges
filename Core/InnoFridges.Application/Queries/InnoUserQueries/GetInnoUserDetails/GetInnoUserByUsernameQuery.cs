﻿using InnoFridges.Domain;
using saja.Queries;

namespace InnoFridges.Application.Queries.InnoUserQueries.GetInnoUserDetails;

public class GetInnoUserByUsernameQuery : GetModelByUsernameQuery<InnoUser>
{
}
﻿using InnoFridges.Domain;
using saja.Interfaces;

namespace InnoFridges.Application.Interfaces;

public interface IInnoUserRepository : IUserModelRepository<InnoUser>
{
    
}